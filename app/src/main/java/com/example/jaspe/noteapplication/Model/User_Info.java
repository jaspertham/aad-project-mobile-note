package com.example.jaspe.noteapplication.Model;

public class User_Info {
    //private username,password,email so that it is not public for other class
    private String pwd;
    private String email;

    public User_Info() {

    }

    public User_Info(String pwd, String email) {
        this.pwd = pwd;
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}



