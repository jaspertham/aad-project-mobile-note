package com.example.jaspe.noteapplication;
/**
 * This section of the code cannot be done with the help a youtube tutorial channel. This channel
 * have lots of great application tutorial that he/she is very kind enough to share to youtube. This
 * is the link to the channel https://www.youtube.com/user/eddydn71
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jaspe.noteapplication.Model.User_Info;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignIn extends AppCompatActivity {
    EditText editUsername,editPassword;
    Button button_sign_in;
    boolean emptyCheck;

    FirebaseDatabase firedatabase; //to use firebase as firedatabase
    DatabaseReference user_table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        editPassword = (EditText)findViewById(R.id.editPassword);
        editUsername = (EditText)findViewById(R.id.editUsername);
        button_sign_in = (Button) findViewById(R.id.button_sign_in);

         firedatabase =  FirebaseDatabase.getInstance();
          user_table = firedatabase.getReference("Users");

        button_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog mDialog = new ProgressDialog(SignIn.this);
                mDialog.setMessage("Processing...");
                mDialog.show();

                user_table.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //Check if user exist
                        if (dataSnapshot.child(editUsername.getText().toString()).exists()) {
                            mDialog.dismiss();
                            //Get user information
                            User_Info user = dataSnapshot.child(editUsername.getText().toString()).getValue(User_Info.class);
                            if (user.getPwd().equals(editPassword.getText().toString())) {
                                Toast.makeText(SignIn.this, "Sign in successful", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(SignIn.this,Note_Application.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(SignIn.this, "Incorrect password!", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            mDialog.dismiss();
                            Toast.makeText(SignIn.this, "This user does not exist!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
    }
}
