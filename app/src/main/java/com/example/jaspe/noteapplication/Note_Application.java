package com.example.jaspe.noteapplication;
/**
 * This section of the code cannot be done with the help of Udemy tutorial website. This is the link
 * to the wonderful tutorial "The Complete Android N Developer Course" by Rob Percival.
 * https://www.udemy.com/complete-android-n-developer-course/learn/v4/overview
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;

public class Note_Application extends AppCompatActivity {

    static  ArrayAdapter arrayAdapter;
    static ArrayList<String> notes = new ArrayList<>();

    //add inflater menu for the top right corner of the note application
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    //event listener on what happened when the inflater is clicked
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       super.onOptionsItemSelected(item);
         if (item.getItemId() == R.id.add_note){
             Intent intent = new Intent(getApplicationContext(),Note_Editor.class);
             startActivity(intent);
             return true;
         }
         return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Note");
        setContentView(R.layout.activity_note__application);


        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.example.jaspe.noteapplication",
                Context.MODE_PRIVATE);

        HashSet<String> set = (HashSet<String>)sharedPreferences.getStringSet("notes",null);
        if (set == null){
            notes.add("Example Note");
        }else {
            notes = new ArrayList(set);
        }

        ListView note_list = (ListView)findViewById(R.id.note_list);
         arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,notes);
        note_list.setAdapter(arrayAdapter);
        //to click on a note
        note_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {

                Intent intent = new Intent(getApplicationContext(),Note_Editor.class);
                intent.putExtra("noteId", i);
                startActivity(intent);

            }
        });
        //to delete note
        note_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int deleteItem = position;
                new AlertDialog.Builder(Note_Application.this)
                        .setIcon(R.drawable.ic_whatshot_black_24dp)
                .setTitle("WARNING!").setMessage("Do you want to delete this?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        notes.remove(deleteItem);
                        arrayAdapter.notifyDataSetChanged();
                        //data persistence
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.example.jaspe.noteapplication",
                                Context.MODE_PRIVATE);
                        HashSet<String> set = new HashSet(Note_Application.notes);
                        sharedPreferences.edit().putStringSet("notes",set).apply();


                    }
                    }
                )
                .setNegativeButton("Cancel",null)
                .show();
                //if it is false, then it will assume a short click instead of long click
                return true;
            }
        });
    }
}
