package com.example.jaspe.noteapplication;

/**
 * This section of the code cannot be done with the help a youtube tutorial channel. This channel
 * have lots of great application tutorial that he/she is very kind enough to share to youtube. This
 * is the link to the channel https://www.youtube.com/user/eddydn71
 */
import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jaspe.noteapplication.Model.User_Info;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignUp extends AppCompatActivity {

    EditText newUsername, newPassword, newEmail;
    Button button_sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        newUsername = (EditText)findViewById(R.id.newUsername);
        newPassword = (EditText)findViewById(R.id.newPassword);
        newEmail = (EditText)findViewById(R.id.newEmail);

        button_sign_up = (Button) findViewById(R.id.button_sign_up);

        final FirebaseDatabase database = FirebaseDatabase.getInstance(); //to use firebase as firedatabase
        final DatabaseReference user_table  = database.getReference("Users");

        button_sign_up.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog = new ProgressDialog(SignUp.this);
                mDialog.setMessage("Registering...");
                mDialog.show();

                user_table.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(newUsername .getText().toString()).exists()) {
                            mDialog.dismiss();
                                Toast.makeText(SignUp.this, "User already exist!",
                                        Toast.LENGTH_SHORT).show();
                                finish();

                        }
                        else {
                            mDialog.dismiss();
                                User_Info user_info = new User_Info(newPassword.getText().toString(), newEmail.getText().toString());
                                user_table.child(newUsername.getText().toString()).setValue(user_info);
                                Toast.makeText(SignUp.this, "Sign up successfully!",
                                        Toast.LENGTH_SHORT).show();

                                finish();
                                user_table.removeEventListener(this);

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }
}
