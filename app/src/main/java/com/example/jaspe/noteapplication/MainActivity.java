package com.example.jaspe.noteapplication;
/**
 * This section of the code cannot be done with the help a youtube tutorial channel. This channel
 * have lots of great application tutorial that he/she is very kind enough to share to youtube. This
 * is the link to the channel https://www.youtube.com/user/eddydn71
 */
import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    Button button_sign_up,button_sign_in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_sign_up = (Button)findViewById(R.id.button_sign_up);
        button_sign_in = (Button)findViewById(R.id.button_sign_in);

        //when sign up button clicked
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUp = new Intent(MainActivity.this,SignUp.class);
                startActivity(signUp);

            }
        });
        //when sign in button clicked
        button_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signIn = new Intent(MainActivity.this,SignIn.class);
                startActivity(signIn);
            }
        });
    }
}
