package com.example.jaspe.noteapplication;
/**
 * This section of the code cannot be done with the help of Udemy tutorial website. This is the link
 * to the wonderful tutorial "The Complete Android N Developer Course" by Rob Percival.
 * https://www.udemy.com/complete-android-n-developer-course/learn/v4/overview
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.HashSet;

public class Note_Editor extends AppCompatActivity {
    int noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note__editor);

        EditText editText = (EditText)findViewById(R.id.editText);
        ImageButton back_button = (ImageButton)findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(getApplicationContext(),Note_Application.class);
                startActivity(back);
            }
        });
        Intent intent = getIntent(); //get intent id
        noteId = intent.getIntExtra("noteId",-1);
        //when a note is clicked, set text of the orginal text so it wont user have to retype
        if(noteId != -1){

            editText.setText(Note_Application.notes.get(noteId));
        }else {
            Note_Application.notes.add("");
            noteId = Note_Application.notes.size() -1;
            Note_Application.arrayAdapter.notifyDataSetChanged();
        }
        //add text change listener so that the user's note is saved after tapping out
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Note_Application.notes.set(noteId, String.valueOf(s));
                Note_Application.arrayAdapter.notifyDataSetChanged();
                //data persistence
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.example.jaspe.noteapplication",
                        Context.MODE_PRIVATE);
                HashSet<String>set = new HashSet(Note_Application.notes);
                sharedPreferences.edit().putStringSet("notes",set).apply();


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
